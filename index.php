<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="css.css" type="text/css">

<body>

    <?php
        $khoas = array("" => '', "MAT" => 'Khoa học máy tính', "KDL" => 'Khoa học dữ liệu');
       
    ?>
    <div class="bg">
    <div class="list_div">
        <div class="row_index">
            <div class="column_label div_tab ">Khoa</div>
            <div class="column_input_index ">
                <select class="div_select_khoa" name="khoa" id="khoa">
                    <?php
                    foreach ($khoas as $khoa) {
                        echo '<option value="' . $khoa . '" name = "khoa">' . $khoa . '</option>';
                    };
                    ?>
                </select>
            </div>
        </div>

        <div class="row_index">
            <div class="column_label div_tab">Từ khóa</div>
            <div class="column_input_index">
                <input class="div_input_tukhoa" name="tukhoa">
            </div>
        </div>

        <div class="">
            <input class="btn_tk div_button_index" type="submit" value="Tìm kiếm" name="submit">
        </div>

    </div>

    <div class = "div_table">
        <table class="table_0" style="width:100%">
            <tr>
                <td colspan = 3>Số sinh viên tìm thấy: XXX</td>
                <td width= "25%">
                    <form action="register.php">
                        <div class="">
                            <input class="center btn_add" type="submit" value="Thêm" name="add">
                        </div>
                    </form>
                </td>
            </tr>

            <tr>
                <td>No</td>
                <td>Tên sinh viên</td>
                <td>Khoa</td>
                <td style=" text-align:center">Action</td>
            </tr>

            <tr>
                <td width="2%" style=" text-align:right">1</td>
                <td width="45%">Nguyễn Văn A</td>
                <td width="28%">Khoa học máy tính</td>
                <td style=" text-align:center" width="25%">
                    <div>
                        <input class=" btn_change_delete" type="submit" value="Xóa" name="delete">
                        <input class=" btn_change_delete" type="submit" value="Sửa" name="change">
                    </div>
                </td>
            </tr>
            
            <tr>
                <td width="2%" style=" text-align:right">2</td>
                <td width="45%">Trần Thị B</td>
                <td width="28%">Khoa học máy tính</td>
                <td style=" text-align:center" width="25%">
                    <div>
                        <input class=" btn_change_delete" type="submit" value="Xóa" name="delete">
                        <input class=" btn_change_delete" type="submit" value="Sửa" name="change">
                    </div>
                </td>
            </tr>
            
            <tr>
                <td width="2%" style=" text-align:right">3</td>
                <td width="45%">Nguyễn Hoàng C</td>
                <td width="28%">Khoa học dữ liệu</td>
                <td style=" text-align:center" width="25%">
                    <div>
                        <input class=" btn_change_delete" type="submit" value="Xóa" name="delete">
                        <input class=" btn_change_delete" type="submit" value="Sửa" name="change">
                    </div>
                </td>
            </tr>

            <tr>
                <td width="2%" style=" text-align:right">4</td>
                <td width="45%">Đinh Quang D</td>
                <td width="28%">Khoa học dữ liệu</td>
                <td style=" text-align:center" width="25%">
                    <div>
                        <input class=" btn_change_delete" type="submit" value="Xóa" name="delete">
                        <input class=" btn_change_delete" type="submit" value="Sửa" name="change">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </div>
    
</body>
