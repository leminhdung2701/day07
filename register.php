<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="css.css" type="text/css">

<body>

    <?php
        $gender = array(0 => 'Nam', 1 => 'Nữ');
        $khoas = array("" => '', "MAT" => 'Khoa học máy tính', "KDL" => 'Khoa học dữ liệu');
        $notifi = array();
        $isImage = False;
        if (!empty($_POST["submit"])) {
            $notifi['tên'] = (!empty($_POST["name"])) ? 1 : 0;
            $notifi['giới tính'] = (!empty($_POST["gender"])) ? 1 : 0;
            $notifi['phân khoa'] = (!empty($_POST["khoa"])) ? 1 : 0;
            $notifi['ngày sinh'] = (!empty($_POST["date"])) ? 1 : 0;
            if (''.$_FILES['imageInput']['tmp_name'].''!=''){
                $isImage = exif_imagetype($_FILES['imageInput']['tmp_name']);
                // $img = file_get_contents($_FILES['imageInput']['tmp_name']);
                // echo '<img class="text" src="data:image/png;base64,'.$img.'" width="50%">';
            }
        };
        
        function upload($imageName){
            $target_dir = 'upload/';
            if (!is_dir($target_dir)) {
                mkdir($target_dir,0777, true);
            }
            $ex = pathinfo($imageName, PATHINFO_EXTENSION);
            $name = pathinfo($imageName, PATHINFO_FILENAME);
            $date = date ("YmdHis");
            $path = $name.'_'.$date.".".$ex;
            $target_file  = $target_dir.basename($path);
            $moved = move_uploaded_file($_FILES['imageInput']['tmp_name'], $target_file);
            if( $moved ) {
                return '/web/day07/upload/'.$path;     
            }   
        //  else {
        //         echo "Not uploaded";
        //     }
        };

    ?>

    <div class="login">
        <div>
            <?php
            if (!empty($_POST["submit"])) {
                $check = True;
                foreach ($notifi as $key => $value) {
                    if ($value == 0) {
                        echo '<div class="div_notfi" style="color:crimson">Hãy nhập ' . $key . '.</div>';
                        $check = False;
                    }
                };

                if(!$isImage){
                    echo '<div class="div_notfi" style="color:crimson">Message validate.</div>';
                    $check = False;
                }

                if($check){
                    session_start();
                    $_SESSION['name'] = $_POST["name"];
                    $_SESSION['gender'] = $_POST["gender"];
                    $_SESSION['khoa'] = $_POST["khoa"];
                    $_SESSION['date'] = $_POST["date"];
                    $_SESSION['address'] = $_POST["address"];
                    // $_SESSION['imageInput'] = file_get_contents($_FILES['imageInput']['tmp_name']);
                    $_SESSION['imageInput'] = upload($_FILES['imageInput']['name']);
                    header('Location: confirm.php');
                }
            }
            ?>
        </div>

        <form method="POST" action="register.php" enctype='multipart/form-data'>
            <div class="row">
                <div class="column_label div_label">Họ và tên<sapn style="color:crimson">*</sapn>
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <input class="div_input" name="name">
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label">Giới tính<sapn style="color:crimson">*</sapn>
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <div class="div_radio row_radio">
                        <?php
                        for ($i = 0; $i < count($gender); $i++) {
                            echo '<input class = "input_radio" type="radio" name="gender" value="' . $gender[$i] . '" >
                            <label class = "radio"  for="html">' . $gender[$i] . '</label><br>';
                        };
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label">Phân khoa<sapn style="color:crimson">*</sapn>
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <select class="div_select" name="khoa" id="khoa">
                        <?php
                        foreach ($khoas as $khoa) {
                            echo '<option value="' . $khoa . '" name = "khoa">' . $khoa . '</option>';
                        };
                        ?>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label">Ngày sinh<sapn style="color:crimson">*</sapn>
                </div>
                <div class="column"> </div>
                <div class="column_input">
                    <input class="input_date" name="date" required type="date" placeholder="dd/mm/yyyy" />
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label">Địa chỉ</div>
                <div class="column"> </div>
                <div class="column_input">
                    <input name="address" class="div_input">
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label">Hình ảnh</div>
                <div class="column"> </div>
                <div class="column_input">
                    <input class="input_img" type="file" name="imageInput">
                </div>
            </div>

            <div class="">
                <input class="center div_button" type="submit" value="Đăng ký" name="submit">
            </div>

        </form>

    </div>
    
</body>